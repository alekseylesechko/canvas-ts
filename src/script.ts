import Field from './components/Field';
import Point from './components/Point';

const areaEl: HTMLElement = document.querySelector('#area');
const perimeterEl: HTMLElement = document.querySelector('#perimeter');
const canvas: HTMLCanvasElement = document.getElementById(
	'canvas'
) as HTMLCanvasElement;
canvas.height = Field.height;
canvas.width = Field.width;
const ctx = canvas.getContext('2d');
const field: Field = new Field(ctx, canvas);

canvas.addEventListener('click', onClick);
document.querySelector('#clear').addEventListener('click', onClear);
document.querySelector('#draw').addEventListener('click', onDraw);

function onClick(event: MouseEvent): void {
	const {x, y}: Point = field.getPoint(event);
	const point = new Point(x, y);
	field.addPoint(point);
}

function onDraw() {
	field.draw();
	areaEl.textContent = field.getArea();
	perimeterEl.textContent = field.getPerimeter();
}

function onClear() {
	field.clear();
	areaEl.textContent = '0';
	perimeterEl.textContent = '0';
}
