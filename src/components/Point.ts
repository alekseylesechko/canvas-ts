import {IPoint} from './interfaces';

export default class Point implements IPoint {

	constructor(public x: number, public y: number) {
	}
}
