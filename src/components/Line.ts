import {ILine} from '../interfaces';
import Point from './Point';

export default class Line implements ILine {
	start: Point;
	end: Point;
	constructor(start: Point, end: Point) {
		this.start = start;
		this.end = end;
	}

	get length(): number {
		return Math.hypot(this.start.x - this.end.x, this.start.y - this.end.y);
	}
}
