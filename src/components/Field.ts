import {IField} from '../interfaces';
import Point from './Point';
import Line from './Line';

export default class Field implements IField {
	points: Point[] = [];
	drawed = false;
	private SM_IN_PX = 0.0264583333;
	private static WIDTH = 900;
	private static HEIGHT = 600;

	static get width(): number {
		return this.WIDTH;
	}

	static get height(): number {
		return this.HEIGHT;
	}

	constructor(
		public ctx: CanvasRenderingContext2D,
		private canvas: HTMLCanvasElement
	) {}

	private fillDot(point: Point): void {
		this.chechField();
		this.ctx.beginPath();
		this.ctx.arc(point.x, point.y, 3, 0, 2 * Math.PI);
		this.ctx.fillStyle = 'red';
		this.ctx.fill();
	}

	fillLine(): void {
		const [first, ...points] = this.points;
		const last = points.splice(points.length - 1)[0];

		this.ctx.beginPath();
		this.ctx.moveTo(first.x, first.y);
		points.forEach((el) => {
			this.ctx.lineTo(el.x, el.y);
		});
		this.ctx.lineTo(last.x, last.y);
		this.ctx.fill();
		this.ctx.closePath();
	}

	getPoint(event: MouseEvent): Point {
		const rect = this.canvas.getBoundingClientRect();
		const x = event.clientX - rect.left;
		const y = event.clientY - rect.top;
		console.log('x: ' + x + ' y: ' + y);
		return {x, y};
	}

	addPoint(point: Point): void {
		this.fillDot(point);
		this.points.push(point);
	}

	draw(): void {
		if (this.points.length < 3) {
			return alert('You need at least three points...');
		}
		this.ctx.fillStyle = 'black';
		this.clearField();
		this.fillLine();
		this.drawed = true;
	}

	chechField(): void {
		if (this.drawed) {
			this.clear();
			this.drawed = false;
		}
	}

	getArea(): string {
		let square = 0;
		const n = this.points.length;
		for (let i = 0; i < n; i++) {
			const nextPoint = this.points[i + 1] ?? this.points[0];
			const {x, y} = this.points[i];
			square += x * nextPoint.y - y * nextPoint.x;
		}

		return (0.5 * Math.abs(square) * this.SM_IN_PX ** 2).toFixed(2);
	}

	getPerimeter(): string {
		let perimeter = 0;
		const n = this.points.length;
		for (let i = 0; i < n; i++) {
			const nextPoint = this.points[i + 1] ?? this.points[0];
			const line = new Line(this.points[i], nextPoint);
			perimeter += line.length;
		}
		return (perimeter * this.SM_IN_PX).toFixed(2);
	}

	clearField(): void {
		this.ctx.clearRect(0, 0, Field.WIDTH, Field.HEIGHT);
	}

	clear(): void {
		this.points = [];
		this.clearField();
	}
}
