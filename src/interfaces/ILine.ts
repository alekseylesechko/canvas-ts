import Point from '../Point';

export interface ILine {
	start: Point;
	end: Point;
	length: number;
}
