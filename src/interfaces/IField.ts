import Point from '../components/Point';

export interface IField {
	ctx: CanvasRenderingContext2D
	points : Point[]

	fillLine(arr: Point[]): void;
	getPoint(event: MouseEvent): Point
}
